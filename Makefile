data/md5_passwords.txt:
	cut -f 2 -d: data/md5.txt.pot > data/md5_passwords.txt

data/md5_salt_passwords.txt:
	cut -f 3 -d: data/md5_w_salt.txt.pot > data/md5_salt_passwords.txt

data/passwords.txt: data/md5_passwords.txt data/md5_salt_passwords.txt
	cat data/md5_passwords.txt data/md5_salt_passwords.txt | sort > data/passwords.txt

data/freqs.txt: data/passwords.txt
	uniq data/passwords.txt | sort | uniq -c | sort -rn > data/freqs.txt

data/unique_passwords.txt: data/passwords.txt
	sort data/passwords.txt | uniq > data/unique_passwords.txt

data/10k.zip:
	wget "https://xato.net/files/10k most common.zip" -O data/10k.zip

data/10k_most_common.txt: data/10k.zip
	unzip data/10k.zip
	mv 10k\ most\ common.txt data/10k_most_common.txt

data/top_10.txt: data/freqs.txt
	head data/freqs.txt > data/top_10.txt

data/analytics.txt: analytics.py data/10k_most_common.txt data/unique_passwords.txt data/passwords.txt
	python analytics.py > data/analytics.txt
