#!/bin/python

from collections import defaultdict

TOP10K_FILE = "data/10k_most_common.txt"
PW_FILE = "data/passwords.txt"

def wordcount(fname, lower=2, upper=12):
    """Returns the distribution of password lengths in fname. Accepts an
    upper and lower length boundary for aggregation purposes.

    """
    freqs = defaultdict(int)
    counter = 0
    with open(fname) as f:
        for password in f:
            length = len(password)
            if lower < length < upper:
                freqs[length] += 1
            elif length >= upper:
                freqs["{0} or more".format(upper)] += 1
            elif length <= lower:
                freqs["{0} or less".format(lower)] += 1
            counter += 1
    return freqs, counter

def passwd_type(fname):
    """Returns the frequencies of different password types in fname"""
    # Build in-memory set of TOP 10K. ~10-20x faster than using a list.
    with open(TOP10K_FILE) as f:
        top10k = set(line.strip() for line in f)
    # Boolean expressions for classifying passwords
    classifiers = {
        "only lowercase": lambda w: all(c.islower() for c in w),
        "only uppercase": lambda w: all(c.isupper() for c in w),
        "only numbers": lambda w: all(w.isdigit() for c in w),
        "palindrome": lambda w: w == w[::-1],
        "in top 10K passwords": lambda w: w in top10k,
        "has spaces": lambda w: " " in w
    }
    # Main logic
    freqs = defaultdict(int)
    counter = 0
    with open(fname) as f:
        for password in f:
            any_matches = False
            for (name, fn) in classifiers.iteritems():
                if fn(password.strip()):
                    freqs[name] += 1
                    any_matches = True
            if not any_matches:
                freqs["not matched"] += 1
            counter += 1
    return freqs, counter
        
if __name__ == "__main__":
    print("\n# OF PASSWORDS BY LENGTH:")
    print("-------------------------\n")
    lengths, l_total = wordcount(PW_FILE)
    for (k, v) in lengths.iteritems():
        print("{0}: {1} ({2:.2%})".format(k ,v, v*1.0/l_total))
    print("\nIN TOTAL: {0}".format(l_total))

    print("\n# OF PASSWORDS BY TYPE:")
    print("-----------------------\n")
    types, t_total = passwd_type(PW_FILE)
    for (k, v) in types.iteritems():
        print("{0}: {1} ({2:.2%})".format(k ,v, v*1.0/t_total))
    print("\nIN TOTAL: {0}".format(t_total))


