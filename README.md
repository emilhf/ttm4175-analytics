# Makeshift data wrangling project v.0.1

As part of TTM4175 @ NTNU.

## Requirements

* The total number of cleartext passwords you obtained from all four hash files.
* A list showing the 10 most popular passwords and their frequency.
* A diagram showing the number of passwords of each length n = 1, 2, . . .
* A diagram showing how many passwords were lowercase only, numbers only, upper
case only, etc. Include as many criteria as find relevant.
* A diagram showing how many of the passwords that you found were present in the
10k most common.txt file you downloaded earlier.
